﻿namespace FootPrints
{
    internal static class PageTokens
    {
        public const string PivotPage = "Pivot";
        public const string MainPage = "Main";
        public const string MapPage = "Map";
    }
}
