﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using FootPrints.Helpers;
using FootPrints.Models;
using FootPrints.Services;

using Prism.Windows.Mvvm;
using Prism.Windows.Navigation;

using Windows.Devices.Geolocation;
using Windows.Devices.SerialCommunication;
using Windows.Foundation;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls.Maps;

namespace FootPrints.ViewModels
{
	public class MapViewModel : ViewModelBase
	{
		// TODO WTS: Set your preferred default zoom level
		private const double DefaultZoomLevel = 20;

		private readonly ILocationService _locationService;

		// TODO WTS: Set your preferred default location if a geolock can't be found.
		private readonly BasicGeoposition _defaultPosition = new BasicGeoposition()
		{
			Latitude = 47.609425,
			Longitude = -122.3417
		};

		private string _mapServiceToken;
		public string MapServiceToken
		{
			get { return _mapServiceToken; }
			set { SetProperty(ref _mapServiceToken, value); }
		}

		private double _zoomLevel;
		public double ZoomLevel
		{
			get { return _zoomLevel; }
			set { SetProperty(ref _zoomLevel, value); }
		}

		private Geopoint _center;
		public Geopoint Center
		{
			get { return _center; }
			set { SetProperty(ref _center, value); }
		}

		private ObservableCollection<MapIcon> _mapIcons = new ObservableCollection<MapIcon>();
		public ObservableCollection<MapIcon> MapIcons
		{
			get { return _mapIcons; }
			set { SetProperty(ref _mapIcons, value); }
		}

		private DispatcherTimer timeCounter;

		private StreamReader streamReader;

		private int locateCount;

		public MapViewModel(ILocationService locationServiceInstance)
		{
			_locationService = locationServiceInstance;
			Center = new Geopoint(_defaultPosition);
			ZoomLevel = DefaultZoomLevel;

			// TODO WTS: Set your map service token. If you don't have one, request from https://www.bingmapsportal.com/
			// MapServiceToken = string.Empty;
		}

		public override async void OnNavigatedTo(NavigatedToEventArgs e, Dictionary<string, object> viewModelState)
		{
			base.OnNavigatedTo(e, viewModelState);
			if(_locationService != null)
			{
				_locationService.PositionChanged += LocationServicePositionChanged;

				var initializationSuccessful = await _locationService.InitializeAsync();

				if(initializationSuccessful)
				{
					await _locationService.StartListeningAsync();
				}

				if(initializationSuccessful && _locationService.CurrentPosition != null)
				{
					Center = _locationService.CurrentPosition.Coordinate.Point;
				}
				else
				{
					Center = new Geopoint(_defaultPosition);
				}
			}

			if(e != null)
			{
				var config = e.Parameter as GpsConfig;
				if(config == null) return;

				string aqs = SerialDevice.GetDeviceSelector();
				var devices = await Windows.Devices.Enumeration.DeviceInformation.FindAllAsync(aqs);
				var device = await SerialDevice.FromIdAsync(devices[0].Id);
				if(device == null) return;

				device.BaudRate = 9600;
				device.DataBits = 8;
				device.StopBits = SerialStopBitCount.One;
				device.Parity = SerialParity.None;
				device.Handshake = SerialHandshake.None;
				device.ReadTimeout = TimeSpan.FromMilliseconds(1000);
				device.WriteTimeout = TimeSpan.FromMilliseconds(1000);

				Stream readStream = device.InputStream.AsStreamForRead(0);
				streamReader = new StreamReader(readStream);

				timeCounter = new DispatcherTimer();
				timeCounter.Interval = TimeSpan.FromSeconds(config.IntervalTime);
				timeCounter.Tick += GetGpsPoint;
				timeCounter.Start();
			}

			//var mapIcon = new MapIcon()
			//{
			//	Location = Center,
			//	NormalizedAnchorPoint = new Point(0.5, 1.0),
			//	Title = "Map_YourLocation".GetLocalized(),
			//	Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/map.png")),
			//	ZIndex = 0
			//};
			//MapIcons.Add(mapIcon);
		}

		public override void OnNavigatingFrom(NavigatingFromEventArgs e, Dictionary<string, object> viewModelState, bool suspending)
		{
			base.OnNavigatingFrom(e, viewModelState, suspending);
			if(_locationService != null)
			{
				_locationService.PositionChanged -= LocationServicePositionChanged;
				_locationService.StopListening();
			}
		}

		private void LocationServicePositionChanged(object sender, Geoposition geoposition)
		{
			if(geoposition != null)
			{
				Center = geoposition.Coordinate.Point;
			}
		}

		private void GetGpsPoint(object sender, object e)
		{
			string[] txtary;
			do
			{
				var text = streamReader.ReadLine();
				txtary = text.Split(',');
			} while(txtary[0] != "$GPGGA");

			var geoposition = new BasicGeoposition()
			{
				Latitude = GpggaToDegree(txtary[2]),//double.Parse(txtary[2])/100,
				Longitude = GpggaToDegree(txtary[4])//double.Parse(txtary[4])/100
			};
			try
			{
				var location = new Geopoint(geoposition);
				Center = location;

				var mapIcon = new MapIcon()
				{
					Location = location,
					NormalizedAnchorPoint = new Point(0.5, 1.0),
					Title = $"軽度{geoposition.Latitude}, 緯度{geoposition.Longitude}",//"Map_YourLocation".GetLocalized(),
					Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/map.png")),
					ZIndex = 0
				};
				MapIcons.Add(mapIcon);
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}

		static double GpggaToDegree(string snum)
		{
			double num = double.Parse(snum);
			num /= 100;
			double min = num % 1;
			double deg = num - min;

			return deg + min/60*100;
		}
	}
}
