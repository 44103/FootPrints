﻿using System;
using System.Windows.Input;
using Prism.Commands;
using Prism.Windows.Mvvm;
using Prism.Windows.Navigation;
using FootPrints.Models;

namespace FootPrints.ViewModels
{
	public class MainViewModel : ViewModelBase
	{
		private int intervalTime = 10;
		public int IntervalTime
		{
			get { return intervalTime; }
			set { SetProperty(ref intervalTime, value); }
		}

		private bool isStarted = false;
		private bool IsStarted
		{
			get { return isStarted; }
			set { SetProperty(ref isStarted, value); }
		}

		private string getGpsDataBtnName = "Start Get GPS DATA";
		public string GetGpsDataBtnName
		{
			get { return getGpsDataBtnName; }
			set { SetProperty(ref getGpsDataBtnName, value); }
		}

		public ICommand GetGpsDataCommand { get; set; }

		private INavigationService navigationService;

		public MainViewModel(INavigationService ns)
		{
			navigationService = ns;

			GetGpsDataCommand = new DelegateCommand(() =>
			{
				IsStarted = !IsStarted;
				GetGpsDataBtnName = IsStarted ? "Stop Get GPS DATA" : "Start Get GPS DATA";

				if(IsStarted)
				{
					GoMapPage();
				}
			});
		}

		public void GoMapPage()
		{
			var config = new GpsConfig(IntervalTime);
			navigationService.Navigate("Map", config);
		}
	}
}