﻿using System;

using FootPrints.ViewModels;

using Windows.UI.Xaml.Controls;

namespace FootPrints.Views
{
	public sealed partial class MainPage : Page
	{
		private MainViewModel ViewModel => DataContext as MainViewModel;

		public MainPage()
		{
			InitializeComponent();
		}
	}
}
