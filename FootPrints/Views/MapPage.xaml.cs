﻿using System;

using FootPrints.ViewModels;

using Windows.UI.Xaml.Controls;

namespace FootPrints.Views
{
    public sealed partial class MapPage : Page
    {
        private MapViewModel ViewModel => DataContext as MapViewModel;

        public MapPage()
        {
            InitializeComponent();
        }
    }
}
