﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootPrints.Models
{
	class GpsConfig
	{
		public int IntervalTime { get; set; }

		public GpsConfig(int interval)
		{
			IntervalTime = interval;
		}
	}
}
